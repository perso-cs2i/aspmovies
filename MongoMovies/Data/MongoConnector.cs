﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoMovies.Data
{
    public class MongoConnector
    {
        #region Constants

        public string DefaultConnectionPattern =
            "mongodb://{0}:{1}/{2}";

        public string SecuritedConnectionPattern =
            "mongodb://{3}:{4}@{0}:{1}/{2}?authSource={5}";

        #endregion

        #region Fields

        private string _connectionString;

        #endregion

        #region Constructors

        public MongoConnector(MongoSettings settings)
        {
            

            // set the connection string according to the username
            if (string.IsNullOrEmpty(settings.Username))
            {
                _connectionString = string.Format(DefaultConnectionPattern,
                    settings.Host, settings.Port, settings.Database);
            }
            else
            {
                _connectionString = string.Format(SecuritedConnectionPattern,
                    settings.Host, settings.Port, settings.Database,
                    settings.Username, settings.Password, settings.AuthenticationDatabase);
            }

            Client = new MongoClient(_connectionString);
            Database = Client.GetDatabase(settings.Database);
        }

        #endregion

        #region Properties
        /// <summary>
        /// The MongoClient
        /// </summary>
        public IMongoClient Client { get; set; }

        /// <summary>
        /// The selected Mongo database
        /// </summary>
        public IMongoDatabase Database { get; set; }

       

        #endregion
        
    }
}
