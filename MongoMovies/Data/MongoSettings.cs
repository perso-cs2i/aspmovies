﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoMovies.Data
{
    public class MongoSettings
    {
        #region Constructors

        public MongoSettings(IConfiguration configuration)
        {
            // get Mongo configuration section
            var mongoSection = configuration.GetSection("Mongo");

            // settings
            Host = mongoSection.GetValue<string>("Host");
            Port = mongoSection.GetValue<int>("Port");
            Username = mongoSection.GetValue<string>("Username");
            Password = mongoSection.GetValue<string>("Password");
            Database = mongoSection.GetValue<string>("Database");
            AuthenticationDatabase = mongoSection.GetValue<string>("AuthenticationDatabase");
        }

        #endregion


        #region Properties

        public string Host { get; set; }
        public int Port { get; set; }
        public string Database { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string AuthenticationDatabase { get; set; }

        #endregion
    }
}
