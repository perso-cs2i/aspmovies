﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoMovies.Models
{
    public class Actor
    {
        [BsonElement("name")]
        public string ActorName { get; set; }

        [BsonElement("facebook_likes")]
        public int FacebookLikes { get; set; }
    }
}
