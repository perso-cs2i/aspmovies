﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoMovies.Models
{
    [BsonIgnoreExtraElements]
    public class MovieSimple
    { 
        [BsonElement("movie_title")]
        public string MovieTitle { get; set; }

        [BsonElement("title_year")]
        public int TitleYear { get; set; }

        [BsonElement("genres")]
        public List<string> Genres { get; set; }

        [BsonElement("actors")]
        public List<Actor> Actors { get; set; }

    }
}
