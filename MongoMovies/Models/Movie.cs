﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoMovies.Models
{
    [BsonIgnoreExtraElements]
    public class Movie
    {
            [BsonElement("movie_title")]
            public string MovieTitle { get; set; }

            [BsonElement("title_year")]
            public int TitleYear { get; set; }

            [BsonElement("color")]
            public string Color { get; set; }

            [BsonElement("director_name")]
            public string DirectorName { get; set; }

            //[BsonElement("num_critic_for_reviews")]
            //public int NumCriticForReviews { get; set; }

            //[BsonElement("duration")]
            //public int Duration { get; set; }

            //[BsonElement("gross")]
            //public int Gross { get; set; }

            //[BsonElement("director_facebook_likes")]
            //public int DirectorFacebookLikes { get; set; }

            //[BsonElement("num_voted_users")]
            //public int NumVotedUsers { get; set; }

            //[BsonElement("cast_total_facebook_likes")]
            //public int CastTotalFacebookLikes { get; set; }

            //[BsonElement("facenumber_in_poster")]
            //public int FacenumberInPoster { get; set; }

            [BsonElement("movie_imdb_link")]
            public string MovieImdbLink { get; set; }

            //[BsonElement("num_user_for_reviews")]
            //public int NumUserForReviews { get; set; }

            [BsonElement("language")]
            public string Language { get; set; }

            [BsonElement("country")]
            public string Country { get; set; }

            [BsonElement("content_rating")]
            public string ContentRating { get; set; }

            //[BsonElement("budget")]
            //public int Budget { get; set; }

            //[BsonElement("imdb_score")]
            //public int ImdbScore { get; set; }

            //[BsonElement("movie_facebook_likes")]
            //public int MovieFacebookLikes { get; set; }

            [BsonElement("genres")]
            public List<string> Genres { get; set; }

            [BsonElement("actors")]
            public List<Actor> Actors { get; set; }

        }
    }

