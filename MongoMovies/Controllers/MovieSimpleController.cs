﻿using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;
using MongoMovies.Data;
using MongoMovies.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MongoMovies.Controllers
{
    public class MovieSimpleController : Controller
    {
        #region Fields

        private readonly MongoConnector _connector;

        #endregion
        #region Constructors

        public MovieSimpleController(MongoConnector connector)
        {
            _connector = connector;
        }

        #endregion

        #region Actions

        public IActionResult Index()
        {
            // connection string
            // Pattern 1 : mongodb://HOST:PORT/DATABASE
            //             mongodb://localhost:27017/imdb
            var connectionString = "mongodb://localhost:27017/imdb";

            // MongoDB client
            var mongoClient = new MongoClient(connectionString);

            // Use database (use imdb)
            var mongoDatabase = mongoClient.GetDatabase("imdb");

            // The Movie collection
            // "movies" : the name of the MongoDB collection 'on the Mongo server)
            // MovieSimple : the type of .NET objects to retrieve from the collection
            var movieCollection = mongoDatabase.GetCollection<MovieSimple>("movies");


            // Count movies
            var total = movieCollection.CountDocuments(m => m.TitleYear == 2010);
            var total2 = movieCollection.CountDocuments(m => true);

            // Test
            ViewBag.Message = "Co ok";
            ViewBag.Total = total;
            ViewBag.Total2 = total2;

            return View();
        }

        public IActionResult List()
        {
            var movieCollection = _connector.Database.GetCollection<MovieSimple>("movies");

            // the first movies
            var movies = movieCollection
                .Find(m => true)
                .SortBy(m => m.MovieTitle)
                .Limit(50)
                .ToList();

            //render View
            ViewBag.Movies = movies;
            return View();
        }

        #endregion


    }
}
